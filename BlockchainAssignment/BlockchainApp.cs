﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace BlockchainAssignment
{
    public partial class BlockchainApp : Form
    {
        BlockChain blockchain;
        Wallet.Wallet myNewWallet;
        String privKey;
        String publicKey;
        int transactionLimit;
        int miningSetting;
        public BlockchainApp()
        {
            InitializeComponent();
            blockchain = new BlockChain();
            // Limit of transactions per block
            transactionLimit = 5;
            mainBlackWindow.Text = "27007990 - New BlockChain Initialised\n";
            mainBlackWindow.Text += blockchain.Blocks[0].Read();
            //mainBlackWindow.Text += "\nAssignment Checklist\n" +
            //                        "[x]\tThreading during PoW\n" +
            //                        "[ ]\tAdjust Difficulty in PoW\n" +
            //                        "[X]\tImplement Mining Settings\n" +
            //                        "\t[X]\tRandom\n" +
            //                        "\t[X]\tGreedy - Highest Fee Transactions First\n" +
            //                        "\t[X]\tAltruistic - Longest wait first\n" +
            //                        "\t[ ]\tAddress Preference\n" +
            //                        "[ ] Own idea: PoS, Smart contracts...?";
            //mainBlackWindow.Text += "\nValidation Checklist\n" +
            //                        "[x]\tSending a transaction with address that does not have enough funds to support it\n" +
            //                        "[x]\tSending a transaction and providing bad private key\n" +
            //                        "[x]\tWrong previous hash\n" +
            //                        "[x]\tWrong hash\n" +
            //                        "[ ]\tInvalidation transactions\n";
        }

        private void generateNewWallet()
        {
            myNewWallet = new Wallet.Wallet(out privKey);
            blockchain.allWallets.Add(myNewWallet);
            publicKey = myNewWallet.publicID;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ButtonPrintBlock_Click(object sender, EventArgs e)
        {
            try
            {
                // Gets the block information according to the number in hashIndex textbox
                mainBlackWindow.Text = blockchain.getBlockInformation(Int32.Parse(textBox_BlockIndex.Text));
            }
            // If information retrieved from hashindex textbox is not a number, this catch handles these exceptions
            catch (FormatException)
            {
                if (textBox_BlockIndex.Text == "")
                    mainBlackWindow.Text = "Enter an index value in the text box below.";
                else
                    mainBlackWindow.Text = $"'{textBox_BlockIndex.Text}' is invalid.\nPlease enter a block index.";
            }

        }

        private void button_generateWallet_Click(object sender, EventArgs e)
        {
            generateNewWallet();
            mainBlackWindow.Text = $"Public Key: {publicKey}\nPrivate Key: {privKey}\nWallets in blockchain: {blockchain.allWallets.Count}";
            textBox_publicKey.Text = publicKey;
            textBox_privateKey.Text = privKey;
        }

        private void button_ValidatePrivateKeys_Click(object sender, EventArgs e)
        {
            if (Wallet.Wallet.ValidatePrivateKey(textBox_privateKey.Text, textBox_publicKey.Text))
                mainBlackWindow.Text = "Keys are valid";
            else
                mainBlackWindow.Text = "Keys are invalid";
        }

        private void button_createTransaction_Click(object sender, EventArgs e)
        {
            try
            {
                // Validate private key
                if (!Wallet.Wallet.ValidatePrivateKey(textBox_privateKey.Text, textBox_publicKey.Text))
                {
                    mainBlackWindow.Text = "Invalid Private Key";
                    return;
                }
                // If the sender wallet's balance has less currency than is trying to send (amount+fee)
                if(myNewWallet.balance < (float.Parse(textBox_amount.Text) + float.Parse(textBox_fee.Text)))
                {
                    mainBlackWindow.Text = "Insufficient funds.\tMissing:\t" + (myNewWallet.balance - (float.Parse(textBox_amount.Text) + float.Parse(textBox_fee.Text))) +
                                        " AssignmentCoin\nWallet Balance:\t" + myNewWallet.balance +
                                        " AssignmentCoin" +
                                        "\nAmount to transfer:\t" + textBox_amount.Text + " AssignmentCoin" +
                                        "\nFee:\t" + textBox_fee.Text;
                    return;
                } 
                // Amount, fee, public key, receiver key passed to generate new Transaction
                Transaction transaction = new Transaction(textBox_publicKey.Text, 
                                                          textBox_receiverKey.Text, 
                                                          float.Parse(textBox_amount.Text), 
                                                          float.Parse(textBox_fee.Text), 
                                                          textBox_privateKey.Text);
                // Adds transaction to transaction pool ready for adding to the blockchain
                blockchain.TransactionPool.Add(transaction);
                // Subtract sending amount and fee from sender wallet
                myNewWallet.balance -= (float.Parse(textBox_amount.Text) + float.Parse(textBox_fee.Text));
                foreach (Wallet.Wallet w in blockchain.allWallets)
                {
                    // Find recipient wallet
                    if(w.publicID == textBox_receiverKey.Text)
                    {
                        // Add balance to recipient wallet
                        w.balance += float.Parse(textBox_amount.Text);
                    }
                }
                
                // Print Transaction to UI black text box
                mainBlackWindow.Text = blockchain.TransactionPool[blockchain.TransactionPool.Count-1].ReadTransaction() + "\n" + 
                                    blockchain.TransactionPool.Count;
            }
            catch (FormatException)
            {
                Console.WriteLine($"Amount: '{textBox_amount.Text}'\t Fee: '{textBox_fee.Text}'\n" +
                                  $"Public Sender Key: {textBox_publicKey.Text}\n" +
                                  $"Private Key: {textBox_privateKey.Text}\n" +
                                  $"Public Receiver Key: {textBox_receiverKey.Text}");
            }
        }

        private void textBox_amount_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox_fee_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox_receiverKey_TextChanged(object sender, EventArgs e)
        {

        }

        private void button_generateNewBlock_Click(object sender, EventArgs e)
        {
            miningSetting = 1;
            if (greedy_radioButton.Checked)
            {
                miningSetting = 2;
            }
            else if (altruistic_radioButton.Checked)
            {
                miningSetting = 3;
            } else if (addressspecific_button.Checked)
            {
                miningSetting = 4;
            }
            // Parameter variables for new block object (Makes it easier to read what's required to make a block)
            String minerAddress = textBox_publicKey.Text;
            if (minerAddress == "")
            {
                mainBlackWindow.Text = "Enter public key / miner address";
                return;
            }
            Block previousBlockInChain = blockchain.GetLastBlock();

            // Pass entire transaction pool for block to pick transactions from according to the mining settings
            Block newBlock = new Block(minerAddress, blockchain.TransactionPool, previousBlockInChain, miningSetting);
            // Add new block to the blockchain
            blockchain.Blocks.Add(newBlock);
            // Remove all elements in block's transactionList from transactionPool in the blockchain
            List<Transaction> removeTransactions = new List<Transaction>();

            for(int i = 0; i < blockchain.TransactionPool.Count; i++)
            {
                if(newBlock.transactionList.Contains(blockchain.TransactionPool[i]))
                {
                    removeTransactions.Add(blockchain.TransactionPool[i]);
                }
            }
            foreach(Transaction t in removeTransactions)
            {
                blockchain.TransactionPool.Remove(t);
            }
            
            //blockchain.TransactionPool = blockchain.TransactionPool.Except(newBlock.transactionList).ToList();
            // Display the newBlock's information to the main window in GUI
            mainBlackWindow.Text = newBlock.Read();
            // Retrieves and stores the mining reward transaction from the newly generated block
            Transaction reward = newBlock.transactionList[newBlock.transactionList.Count - 1];
            // Adds reward amount to wallet address that mined/generated the new block.
            foreach(Wallet.Wallet w in blockchain.allWallets)
            {
                if(w.publicID == reward.RecipientAddress)
                {
                    w.balance += reward.Amount;
                }
            }
            // Adds transaction to transaction pool ready for adding to the blockchain
            // blockchain.TransactionPool.Add(reward);
        }

        private void button_ReadAll_Click(object sender, EventArgs e)
        {
            mainBlackWindow.Text = blockchain.getAllBlocksInformation();
        }

        private void button_ReadPendingTransactions_Click(object sender, EventArgs e)
        {
            String pendingTransactions = "";
            foreach (Block b in blockchain.Blocks)
            {
                if (b.transactionList.Count == 0)
                    continue;
                pendingTransactions += b.Read();
            }
            if (pendingTransactions == "") pendingTransactions = "No pending transactions";
            mainBlackWindow.Text = pendingTransactions;
        }

        private void button_BlockchainValidation_Click(object sender, EventArgs e)
        {
            String valid = "";
            
            // Validate blockchain function takes boolean determining if returned information is detailed or not.
            valid += blockchain.validateBlockchain(true);
            // Checks if the private key and public key fields are appropriately connected
            if (!Wallet.Wallet.ValidatePrivateKey(textBox_privateKey.Text, textBox_publicKey.Text))
            {
                valid += "\nPrivate key: \t✗\n";
            }
            else
            {
                valid += "\nPrivate key: \t✓\n";
            }
            mainBlackWindow.Text = valid;
        }

        private void button_CheckBalance_Click(object sender, EventArgs e)
        {
            // Finds wallet and appends public address and balance
            String readWallet = "";
            String receivedFunds = "";
            String sentFunds = "";

            foreach(Wallet.Wallet w in blockchain.allWallets)
            {
                if(w.publicID == textBox_publicKey.Text)
                {
                    readWallet += "Address: " + w.publicID + "\nBalance: " + w.balance + " AssignmentCoin\n\n";
                    // For every block in the blockchain
                    foreach (Block b in blockchain.Blocks)
                    {
                        // For every transaction in this block
                        foreach (Transaction t in b.transactionList)
                        {
                            // Accumulates all transactions where this wallet received funds
                            if (t.RecipientAddress == w.publicID)
                            {
                                receivedFunds += t.ReadTransaction();
                            }
                            // Accumulates all transactions where this wallet sent funds
                            if(t.SenderAddress == w.publicID)
                            {
                                sentFunds += t.ReadTransaction();
                            }
                        }
                    }
                }
            }
            if(sentFunds == "")
            {
                sentFunds += "This user has no transactions sending funds";
            }
            if(receivedFunds == "")
            {
                receivedFunds += "This user has no transactions receiving funds";
            }
            mainBlackWindow.Text = readWallet + "Received funds\n"+ receivedFunds + "Sent funds\n" + sentFunds;
        }

        private void button_badBlock_Click(object sender, EventArgs e)
        {
            List<Transaction> transactions = new List<Transaction>();
            int count = 0;
            foreach (Transaction t in blockchain.TransactionPool)
            {
                if (count == 5) break;
                transactions.Add(t);
                count += 1;
            }
            Random rnd = new Random();
            int randBlock = rnd.Next(4);
            Block badBlock;
            // Generates a bad block - There are various reasons why a block would be considered invalid
            // I'm trying to cover all instances that can be randomly tested.
            Console.Write("Bad Block:\t");
            switch (randBlock)
            {
                // Previous block is actually a random block from blockchain - will not represent previous block. (Excluding topmost block as that's the actual previous block)
                case 0:
                    Console.WriteLine("Bad Prev Block");
                    badBlock = new Block(textBox_publicKey.Text, transactions, blockchain.Blocks[new Random().Next(blockchain.Blocks.Count-1)], miningSetting);
                    blockchain.Blocks.Add(badBlock);
                    mainBlackWindow.Text = badBlock.Read();
                    break;
                // Bad transactions
                case 1:
                    if(transactions.Count == 0)
                    {
                        Console.WriteLine("No Transactions to create bad block with.");
                        return;
                    }

                    Console.WriteLine("Bad Transaction");
                    // Replace hash with bad hash - retrieves rand transaction from list
                    int randTransaction = transactions.Count == 0 ? 0 : rnd.Next(transactions.Count - 1);
                    transactions[randTransaction].Hash = transactions[new Random().Next(transactions.Count - 1)].badHash();
                    // send transactions with bad transaction to new block object
                    badBlock = new Block(textBox_publicKey.Text, transactions, blockchain.GetLastBlock(), miningSetting);
                    blockchain.Blocks.Add(badBlock);
                    mainBlackWindow.Text = badBlock.Read();
                    break;
                case 2:
                    Console.WriteLine("Bad prevHash");
                    // Make legit block
                    badBlock = new Block(textBox_publicKey.Text, transactions, blockchain.GetLastBlock(), miningSetting);
                    // Give it random hash
                    badBlock.prevHash = badBlock.hash;
                    blockchain.Blocks.Add(badBlock);
                    mainBlackWindow.Text = badBlock.Read();
                    break;
                case 3:
                    Console.WriteLine("Bad Hash");
                    // Make legit block
                    badBlock = new Block(textBox_publicKey.Text, transactions, blockchain.GetLastBlock(), miningSetting);
                    // replace hash random hash
                    badBlock.hash = badBlock.prevHash;
                    blockchain.Blocks.Add(badBlock);
                    mainBlackWindow.Text = badBlock.Read();
                    break;
            }
            return;
        }

        private void button_GenerateRandomData_Click(object sender, EventArgs e)
        {
            mainBlackWindow.Text = "\n\n\n\n\n\n\n\n\n\n\n\t\t\t\tGenerated a bunch of randomly generated blockchain info.";
            Random rnd = new Random();

            int randBlocks = rnd.Next(5, 10);
            int randTransactions = rnd.Next(15, 25);
            int randWallets = rnd.Next(5, 10);

            for(int i = 0; i < randWallets; i++)
            {
                generateNewWallet();
            }
            for (int i = 0; i < randBlocks; i++)
            {
                int miningSetting = 1;
                if (greedy_radioButton.Checked)
                {
                    miningSetting = 2;
                }
                else if (altruistic_radioButton.Checked)
                {
                    miningSetting = 3;
                }
                
                // Parameter variables for new block object (Makes it easier to read what's required to make a block)
                String minerAddress = textBox_publicKey.Text;
                if (minerAddress == "")
                {
                    mainBlackWindow.Text = "Enter public key / miner address";
                    return;
                }
                Block previousBlockInChain = blockchain.GetLastBlock();

                // Pass entire transaction pool for block to pick transactions from according to the mining settings
                Block newBlock = new Block(minerAddress, blockchain.TransactionPool, previousBlockInChain, miningSetting);
                // Add new block to the blockchain
                blockchain.Blocks.Add(newBlock);
            }
            for(int i = 0; i < randTransactions; i++)
            {
                int amount = rnd.Next(10);
                float fee = amount / 100;
                
                try
                {
                    // Validate private key
                    if (!Wallet.Wallet.ValidatePrivateKey(textBox_privateKey.Text, textBox_publicKey.Text))
                    {
                        mainBlackWindow.Text = "Invalid Private Key";
                        return;
                    }
                    // If the sender wallet's balance has less currency than is trying to send (amount+fee)
                    if (myNewWallet.balance < (amount + fee))
                    {
                        mainBlackWindow.Text = "Insufficient funds.\tMissing:\t" + (myNewWallet.balance - ((amount) + (fee))) +
                                            " AssignmentCoin\nWallet Balance:\t" + myNewWallet.balance +
                                            " AssignmentCoin" +
                                            "\nAmount to transfer:\t" + amount + " AssignmentCoin" +
                                            "\nFee:\t" + fee;
                        return;
                    }
                    // Amount, fee, public key, receiver key passed to generate new Transaction
                    Transaction transaction = new Transaction(textBox_publicKey.Text,
                                                              textBox_receiverKey.Text,
                                                              (amount),
                                                              (fee),
                                                              textBox_privateKey.Text);
                    // Adds transaction to transaction pool ready for adding to the blockchain
                    blockchain.TransactionPool.Add(transaction);
                    // Subtract sending amount and fee from sender wallet
                    myNewWallet.balance -= ((amount) + (fee));
                    foreach (Wallet.Wallet w in blockchain.allWallets)
                    {
                        // Find recipient wallet
                        if (w.publicID == textBox_receiverKey.Text)
                        {
                            // Add balance to recipient wallet
                            w.balance += amount;
                        }
                    }

                    // Print Transaction to UI black text box
                    mainBlackWindow.Text = blockchain.TransactionPool[blockchain.TransactionPool.Count - 1].ReadTransaction() + "\n" +
                                        blockchain.TransactionPool.Count;
                }
                catch (FormatException)
                {
                    Console.WriteLine($"Amount: '{amount}'\t Fee: '{fee}'\n" +
                                      $"Public Sender Key: {textBox_publicKey.Text}\n" +
                                      $"Private Key: {textBox_privateKey.Text}\n" +
                                      $"Public Receiver Key: {textBox_receiverKey.Text}");
                }
            }
            mainBlackWindow.Text += $"\n\n\t\t\t\t\t\tBlocks : {blockchain.Blocks.Count}" +
                                 $"\n\t\t\t\t\t\tTransactions : {blockchain.TransactionPool.Count}" +
                                 $"\n\t\t\t\t\t\tWallets : {blockchain.allWallets.Count}";

        }

        private void label_receiverKey_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox_publicKey_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox_privateKey_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (int j = 0; j < 5; j++)
            {
                //// Parameter variables for new block object (Makes it easier to read what's required to make a block)
                String minerAddress = textBox_publicKey.Text;
                if (minerAddress == "")
                {
                    mainBlackWindow.Text = "Enter miner address";
                    return;
                }
                List<Transaction> chosenTransactions = new List<Transaction>();
                Console.WriteLine("Block " + blockchain.Blocks.Count);
                Block previousBlockInChain = blockchain.GetLastBlock();
                // Ternarary operator - sets the limit to number of transactions that can be appended to a single block
                // Limit to 5 transactions max per block (Unless there are less than transactionLimit's transactions in the transactionPool)
                int tempLimit = transactionLimit;
                if (transactionLimit > blockchain.TransactionPool.Count)
                {
                    tempLimit = blockchain.TransactionPool.Count;
                }
                // Populates the chosenTransactions list, ready for the block's creation
                for (int i = 0; i < tempLimit; i++)
                    // Add bottom-most transaction from transactionPool
                    chosenTransactions.Add(blockchain.TransactionPool[i]);

                Block newBlock = new Block(minerAddress, chosenTransactions, previousBlockInChain, miningSetting);
                // Add new block to the blockchain
                blockchain.Blocks.Add(newBlock);
                // Remove all elements in chosenTransactions from transactionPool in the blockchain
                blockchain.TransactionPool = blockchain.TransactionPool.Except(chosenTransactions).ToList();
                // Display the newBlock's information to the main window in GUI
                mainBlackWindow.Text = newBlock.Read();
                // Retrieves and stores the mining reward transaction from the newly generated block
                Transaction reward = newBlock.transactionList[newBlock.transactionList.Count - 1];
                // Adds reward amount to wallet address that mined/generated the new block.
                foreach (Wallet.Wallet w in blockchain.allWallets)
                {
                    if (w.publicID == reward.RecipientAddress)
                    {
                        w.balance += reward.Amount;
                    }
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<Transaction> chosenTransactions = new List<Transaction>();
            int tempLimit = transactionLimit;
            if (transactionLimit > blockchain.TransactionPool.Count)
            {
                tempLimit = blockchain.TransactionPool.Count;
            }
            for (int i = 0; i < tempLimit; i++)
                // Add bottom-most transaction from transactionPool
                chosenTransactions.Add(blockchain.TransactionPool[i]);

            String minerAddress = textBox_publicKey.Text;
            Console.WriteLine($"\nNew Block Being Made");
            Console.WriteLine("Non-Threaded vs. Threaded");
            Block newBlock = new Block(minerAddress, chosenTransactions, blockchain.GetLastBlock(), miningSetting);
            //Console.WriteLine($"{newBlock.Read()}\n\n");
            //Thread t = new Thread(newBlock.testMiningFunctions);
            //t.Start();
            //t.Join();
            //newBlock.testMiningFunctions();
            newBlock.testThreadMining();
        }

        private void label1_Click_2(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void altruistic_radioButton_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void random_radioButton_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void radioButton1_CheckedChanged_1(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged_2(object sender, EventArgs e)
        {

        }
    }
}
