﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BlockchainAssignment
{
    class Block
    {
        public DateTime timestamp;
        public int index;
        public int prevIndex;
        public String hash;
        public String prevHash;
        public String minerAddress;
        public String merkleRoot;
        public List<Transaction> transactionList = new List<Transaction>();
        public int nonce = 0;
        public int blockTime = 20;
        public static float difficulty = 7;
        public float reward;
        public float fee;
        public Boolean hashFound = false;
        // Mining Settings
        // 1 - Random
        // 2 - Greedy
        // 3 - Altruistic
        private int miningSetting = 1;
        private int transactionLimit = 5; // Limit transactions stored to each block
        
        // Genesis Block (First block of blockchain)
        public Block()
        {
            prevHash = "";
            prevIndex = 0;
            index = 0;
            //Mine();
            timestamp = DateTime.Now;
            ThreadMine();
        }
        
        public Block(String minerAddress, List<Transaction> transactions, Block lastBlock, int miningSetting)
        {
            this.miningSetting = miningSetting;
            // Initial reward value.
            this.reward = 100;
            // Add transactions from transaction pool to newly generated block's transactionList
            getTransactions(transactions);
            // Public address of the person who mined this block
            this.minerAddress = minerAddress;
            // Adds a reward transaction to transactionList
            transactionList.Add(calculateReward());
            
            timestamp = DateTime.Now;

            // Merkle Root hash based on transactions in transaction list
            this.merkleRoot = calculateMerkleRoot(transactionList);
            index = lastBlock.index + 1;
            // Hash of prior block
            prevHash = lastBlock.hash;
            // Index of prior block
            prevIndex = lastBlock.index;
            // Threaded Mine function
            ThreadMine();
            // Non-Threaded Mine function
            //Mine();
        }

        // Clone block constructor
        public Block(Block block)
        {
            // Initial reward value.
            this.reward = block.reward;
            // Add transactions from transaction pool to newly generated blocks. (before hash)
            transactionList = block.transactionList;
            this.minerAddress = block.minerAddress;
            // Adds a reward transaction to transactionList
            transactionList.Add(calculateReward());
            timestamp = block.timestamp;
            // Recalculates hash
            ThreadMine();
            // Recalculates merkle root
            this.merkleRoot = calculateMerkleRoot(transactionList);
            index = block.index;
            // Hash of prior block
            prevHash = block.prevHash;
            // Index of prior block
            prevIndex = block.prevIndex;
        }

        private String CreateHash()
        {
            SHA256 hasher = SHA256Managed.Create();
            String input = index.ToString() + timestamp.ToString() + prevHash + nonce + difficulty + reward + merkleRoot; ;

            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes((input)));
            
            String hash = string.Empty;

            foreach (byte x in hashByte)
            {
                hash += String.Format("{0:x2}", x);
            }
            return hash;
        }


        private String CreateHash(int eNonce)
        {
            SHA256 hasher = SHA256Managed.Create();
            String input = index.ToString() + timestamp.ToString() + prevHash + eNonce + difficulty + reward + merkleRoot; ;

            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes((input)));

            String hash = string.Empty;

            foreach (byte x in hashByte)
            {
                hash += String.Format("{0:x2}", x);
            }
            return hash;
        }

        private void getTransactions(List <Transaction> transactions)
        {
            // If there are less transactions in the pool than the limit, we set the limit to transactions count.
            if (transactionLimit > transactions.Count) transactionLimit = transactions.Count;
            switch(miningSetting)
            {
                // Random
                case 1:
                    Console.WriteLine("Random Mining");
                    Random rnd = new Random();
                    for(int i = 0; i < transactionLimit; i++)
                    {
                        int r = rnd.Next(0, transactions.Count-1); //for ints
                        Console.WriteLine("Have randomly chosen transaction " + r + "/" + transactions.Count);
                        transactionList.Add(transactions[r]);
                    }
                    break;
                // Greedy (Prioritises transactions with larger amounts of cryptocurrency)
                case 2:
                    Console.WriteLine("Greedy Mining");
                    for(int i = 0; i < transactionLimit; i++)
                    {
                        Transaction tmp = new Transaction();
                        foreach(Transaction t in transactions)
                        {   
                            // Checks for highest amount and replaces transaction if highest amount has been found
                            if (t.Amount > tmp.Amount)
                            {
                                // Stops transactions being stored to block more than once
                                if(transactionList.Contains(t))
                                {
                                    continue;
                                }
                                tmp = t;
                            }
                        }
                        transactionList.Add(tmp);
                    }
                    // Prints evidential info for greedy method of mining
                    foreach(Transaction t in transactions)
                    {
                        if (!transactionList.Contains(t))
                        {
                            Console.WriteLine("Transaction " + transactions.IndexOf(t) + " amount:\t" + t.Amount + "\tNot Chosen");
                        }
                        else
                        {
                            Console.WriteLine("Transaction " + transactions.IndexOf(t) + " amount:\t" + t.Amount + " AssignmentCoin\tChosen");
                        }
                    }
                    break;
                // Altruistic ()
                case 3:
                    Console.WriteLine("Altruistic Mining");
                    for (int i = 0; i < transactionLimit; i++)
                    {
                        Transaction tmp = new Transaction();
                        foreach (Transaction t in transactions)
                        {
                            if (t.Timestamp < tmp.Timestamp)
                            {
                                tmp = t;
                            }
                        }
                        transactionList.Add(tmp);
                    }
                    // Prints evidential info for greedy method of mining
                    foreach (Transaction t in transactions)
                    {
                        if (!transactionList.Contains(t))
                        {
                            Console.WriteLine("Transaction " + transactions.IndexOf(t) + " Time:\t" + t.Timestamp + "\tNot Chosen");
                        }
                        else
                        {
                            Console.WriteLine("Transaction " + transactions.IndexOf(t) + " Time:\t" + t.Timestamp + "\tChosen");
                        }
                    }
                    break;
                case 4:
                    for (int i = 0; i < transactionLimit; i++)
                    {
                        Transaction tmp = new Transaction(); // Blank transaction
                        foreach (Transaction t in transactions)
                        {
                            if (t.SenderAddress == minerAddress || t.RecipientAddress == minerAddress)
                            {
                                tmp = t;
                            }
                        }
                        transactionList.Add(tmp);
                    }
                    break;
            }
        }
        
        private void Mine()
        {
            Console.WriteLine($"%%%%%% Non-Threaded Mine %%%%%%%");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            // Generate string with as many zeroes at the length of the difficulty value
            String hashZeros = "";
            for(int i = 0; i < difficulty; i++)
            {
                hashZeros += "0";
            }
            String h = "";
            // Stores hash with a best difficulty lower than the required difficulty.
            String tmpZeros = "0";
            String tmpHash = "";
            int tmpNonce = 0;
            int iter = 0;
            while (!h.StartsWith(hashZeros))
            {
                // Iterate nonce value every loop the hash does not begin with the string value stored in hashZeroes
                nonce += 1;
                h = CreateHash();
                if (h.StartsWith(tmpZeros))
                {
                    tmpHash = h;
                    tmpZeros = "";
                    foreach (char c in tmpHash)
                    {
                        if (c == '0')
                        {
                            tmpZeros += "0";
                        }
                        else
                        {
                            break;
                        }
                    }
                    tmpHash = h;
                    tmpNonce = nonce;
                    // If blockTime has been exceeded - return next best hash with lower difficulty satisfaction
                    if (sw.ElapsedMilliseconds >= blockTime * 1000)
                    {
                        if (tmpZeros.Length < difficulty)
                        {
                            difficulty = tmpZeros.Length;
                            Console.Write("Got Difficulty " + tmpZeros.Length + " within " + blockTime + " seconds block time\n");
                        }
                        h = tmpHash;
                        nonce = tmpNonce;
                        break;
                    }
                }
            }
            //Console.WriteLine(index.ToString() + "\n" + timestamp.ToString() + "\n" + prevHash + "\n" + nonce + "\n" + difficulty + "\n" + reward + "\n" + merkleRoot);
            hash = h;
            sw.Stop();
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = sw.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}.{1:00}", ts.Seconds, ts.Milliseconds/10);
            // Returns hash that fulfils the difficulty rule
            Console.WriteLine("Took " + elapsedTime + "s to generate hash.\nDifficulty: " + difficulty + $"\nNonce {nonce}\nHash: {this.hash}\n");
        }

        /**
         * Reference: https://docs.microsoft.com/en-us/dotnet/api/system.threading.tasks.parallel?view=net-5.0
         * Parallel loop using tasks to get hash - not a great solution as it finds other hashes that satisfy the difficulty
         */
        private void ParallelLoopMine()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int N = 100000000;
            String temp = "";
            String hashZeros = "";
            List<String> tempHashes = new List<string>();
            for(int i = 0; i < difficulty; i++)
            {
                hashZeros += "0";
            }
            Parallel.For(0, N, (i, state) =>
            {
                // Increment nonce value before generating hash.
                if (!temp.StartsWith(hashZeros))
                {
                    nonce += 1;
                    if (nonce != 0 && nonce % 1000000 == 0)
                    {
                        Console.WriteLine("PMine Nonce: " + nonce + "\t" + i);
                    }
                    temp = !temp.StartsWith(hashZeros) ? CreateHash() : temp;
                } else if (temp.StartsWith(hashZeros) || !this.hashFound) 
                {
                    // hash has been found
                    if(tempHashes.Count < 1)
                    {
                        tempHashes.Add(temp);
                        Console.WriteLine("Found: " + temp);
                        this.hashFound = true;
                        Console.WriteLine("Found2: " + temp);
                    }
                    // Break out of this threaded for loop
                    state.Break();
                    sw.Stop();
                } 
            });
            Console.WriteLine("No Of Hashes: " + tempHashes.Count);
            Console.WriteLine(index.ToString() + "\n" + timestamp.ToString() + "\n" + prevHash + "\n" + nonce + "\n" + difficulty + "\n" + reward + "\n" + merkleRoot);
            this.hash = tempHashes[0];
            Console.WriteLine("Hash: " + this.hash + "\ntempHashes[0]" + tempHashes[0]);
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = sw.Elapsed;
            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}.{1:00}", ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("Took " + elapsedTime + "s to generate hash.\nDifficulty: " + difficulty + $"\nNonce {nonce}\nHash: {this.hash}");
        }

        public delegate void ParamaterizedThreadStart(object obj);
        
        private void ThreadMine()
        {
            Console.WriteLine($"%%%%%% Threaded Mine %%%%%%%");
            //Console.WriteLine($"\nThreading starting\nBlock:\n{this.Read()}");
            String tHash = "";
            String hashZeros = "";
            String tmpZeros = "0";
            for (int i = 0; i < difficulty; i++)
            {
                hashZeros += "0";
            }
            hashFound = false;
            Stopwatch sw = new Stopwatch();
            // Start stopwatch
            sw.Start();
            int eNonce = 0;
            int tmpNonce = 0;
            object threadENonce = eNonce;
            // Makes a reasonable number of threads based on processor count
            Thread[] threads = new Thread[Environment.ProcessorCount];
            // For every thread (In my case, 4)
            for (int i = 0; i < threads.Length; i++)
            {
                // Assign each thread this function
                threads[i] = new Thread(() =>
                {
                    String tempHash = "";
                    while (!tempHash.StartsWith(hashZeros))
                    {
                        if(!hashFound)
                        {
                            eNonce += hashFound ? 0 : 1;
                            tempHash = CreateHash(eNonce);

                            if (tempHash.StartsWith(tmpZeros))
                            {
                                tmpZeros = "";
                                foreach (char c in tempHash)
                                {
                                    if (c == '0')
                                    {
                                        tmpZeros += "0";
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                                tHash = tempHash;
                                tmpNonce = eNonce;
                            }
                            // If blockTime has been exceeded - return next best hash with lower difficulty satisfaction
                            if (sw.ElapsedMilliseconds >= blockTime * 1000)
                            {
                                hash = tHash;
                                hashFound = true;
                                nonce = tmpNonce;
                                return;
                            }

                            if (tempHash.StartsWith(hashZeros) && !hashFound)
                            {
                                lock (threadENonce);
                                nonce = eNonce;
                                hash = tempHash;
                                hashFound = true;
                                break;
                            }
                        }
                        else
                        {
                            return;
                        }
                    }
                    // Console.WriteLine($"Found hash: {hash}");
                    return;
                });
                //threads[i].Name = "Thread " + i;
            };
            foreach(Thread t in threads)
            {
                // Start each thread to search for the hash
                t.Start();
            }
            foreach (Thread t in threads)
            {
                // Wait for other threads to finish when one has found a result
                t.Join();
            }
            
            //Console.WriteLine($"Beginning Nonce: {nonce}\t Hash: {this.hash}");
            sw.Stop();
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = sw.Elapsed;
            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}.{1:00}", ts.Seconds, ts.Milliseconds / 10);
            if (tmpZeros.Length < difficulty)
            {
                //difficulty = tmpZeros.Length;
                Console.Write("Missed Difficulty: " + difficulty + "\nGot Difficulty " + tmpZeros.Length + " within the set " + blockTime + " seconds block time\n");
            } else
            {
                Console.WriteLine("Took " + elapsedTime + "s to generate hash.\nDifficulty: " + difficulty + $"\nNonce {nonce}\nHash: {this.hash}\n");
            }
        }
        
        /**
         * Generates hashes using the same information but one method uses threading, the other doesn't.
         * Mine() : Non-Threaded
         * ParallelLoopMine() : Threaded
         */
        public void testMiningFunctions()
        {
            this.hash = "";
            nonce = 0;
            Console.WriteLine("Non-Threaded vs. Threaded");
            Mine();
            this.hash = "";
            nonce = 0;
            ParallelLoopMine();
            Console.WriteLine("Testing done");
        }
        
        public void testThreadMining()
        {
            Console.WriteLine(this.Read());
            // Reset nonce and hash to default values
            this.nonce = 0;
            this.hash = "";
            // Generate hash using non-threaded mine
            Mine();
            Console.WriteLine($"{this.Read()}"); // Read block to compare with previously generated hash for this block
            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            //Console.WriteLine($"Hash found: {this.hash}\nNonce: {nonce}\nThreaded mining finished");
        }
        
        public void rehash()
        {
            String oldHash = this.hash;
            Mine();
            Console.WriteLine($"Old hash: {oldHash}\nNew Hash: {this.hash}");
        }

        private Transaction calculateReward()
        {
            // For every transaction's amount and fee, add to rewardAndFees
            if (transactionList.Count > 0)
            {
                foreach (Transaction t in transactionList)
                {
                    // Append this transactions fee value to reward
                    //this.reward += t.Fee;
                    // This blocks transaction fees
                    this.fee += t.Fee;
                }
            }
            // Add collective amounts to transactionList
            return new Transaction("Mine Rewards", minerAddress, this.reward, 0, "");
        }

        public static String calculateMerkleRoot(List<Transaction> transactionList)
        {
            // Initialise a list of all hashes from this blocks transactions
            List<String> hashes = new List<String>();
            foreach(Transaction t in transactionList)
            {
                hashes.Add(t.Hash);
            }
            // To temporarily store newly generated hashes
            List<String> tempHashes = new List<String>();
            String tempHash = "";

            // If only one hash, return hash result from combineHash function
            if (hashes.Count == 1) 
                return HashCode.HashTools.combineHash(hashes[0], hashes[0]);

            // Continue inner code until hashes list only has 1 element (Merkle Root)
            while(hashes.Count != 1)
            {
                Console.WriteLine(hashes.Count);
                for(int i = 0; i < hashes.Count; i+=2)
                {
                    if(i == (hashes.Count-1))
                    {
                        tempHash = HashCode.HashTools.combineHash(hashes[i], hashes[i]);
                    }
                    else
                    {
                        String currentHash = hashes[i];
                        String nextHash = hashes[i + 1];
                        tempHash = HashCode.HashTools.combineHash(hashes[i], hashes[i + 1]);

                        hashes.Remove(currentHash);
                        hashes.Remove(nextHash);
                    }
                    // Add new temphash to temphashes array
                    tempHashes.Add(tempHash);
                }
                
                // Hashes becomes tempHashes (Contains the newly generated hashes)
                hashes = tempHashes;
            }
            // Will return merkle root once there's only one element left in the hashes array
            return hashes[0];
        }

        public String Read()
        {
            String output = "";
            output += "Block Index: " + this.index +
                        "\tTimeStamp: " + this.timestamp +
                        "\nHash:\t\t" + this.hash +
                        "\nMerkle Root:\t" + this.merkleRoot +
                        "\nPrevious Hash:\t" + this.prevHash +
                        "\nNonce:\t" + this.nonce +
                        "\nDifficulty:\t" + difficulty +
                        "\nReward:\t" + this.reward + "\tFees:\t" + this.fee +
                        "\nTotal Reward:\t" + (this.reward + this.fee) +
                        "\nTransactions:\t" + this.transactionList.Count + "\n";


            //foreach (Transaction t in transactionList)
            //{
            //    output += "\n" + t.ReadTransaction();
            //}
            return output;
                   
        }
    }
}
