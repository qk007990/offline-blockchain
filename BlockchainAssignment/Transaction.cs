﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BlockchainAssignment
{
    class Transaction
    {
        public String Hash;
        public String Signature;
        public String SenderAddress;
        public String RecipientAddress;
        public DateTime Timestamp;
        public float Amount;
        public float Fee;

        public Transaction() { }
        // Amount = amount sending
        // Fee = fee required to send
        // senderaddress = who the amount is from
        // recipientaddress = who the amount is being sent to
        public Transaction(String senderaddress,  String recipientaddress, float amount, float fee, String senderprivatekey)
        {
            // Assign transaction parameters to corresponding variables
            SenderAddress = senderaddress;
            RecipientAddress = recipientaddress;
            Amount = amount;
            Fee = fee;
            // Generate has, timestamp and signature
            Hash = CreateHash();
            Signature = Wallet.Wallet.CreateSignature(SenderAddress, senderprivatekey, Hash);
            Timestamp = DateTime.Now;
        }
        public String CreateHash()
        {
            SHA256 hasher;
            hasher = SHA256Managed.Create();
            String input = SenderAddress.ToString() + RecipientAddress.ToString() + Amount.ToString() + Fee.ToString();
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes((input)));

            String hash = string.Empty;

            foreach (byte x in hashByte)
            {
                hash += String.Format("{0:x2}", x);
            }
            return hash;
        }

        /**
         * Generates bad hash for validation purposes.
         * TODO: Make a more concrete method for rehash 
         */
        public String badHash()
        {
            // Converts hash to char array
            char[] array = Hash.ToCharArray();
            // Initialise random object
            Random rng = new Random();
            // Save array length
            int n = array.Length;
            // While the array's length is more than 1
            while (n > 1)
            {
                n--;
                // Random num between 0 and n+1
                int k = rng.Next(n + 1);
                // temp value to hold char array val
                var value = array[k];
                // Swap values
                array[k] = array[n];
                // Gives array[n] array[k] old value
                array[n] = value;
            }
            return new string(array);
        }
        public String ReadTransaction()
        {
            return $"\nTransaction Hash: \t{Hash}\n" +
            $"Digital Signature: \t{Signature}\n" +
            $"Timestamp: \t{Timestamp}\n" +
            $"Transferred: \t{Amount} AssignmentCoin\n" +
            $"Fees: \t\t{Fee}\n" +
            $"Sender Address: \t{SenderAddress}\n" +
            $"Receiver Address: \t{RecipientAddress}\n\n";
        }
    }
}
