﻿namespace BlockchainAssignment
{
    partial class BlockchainApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainBlackWindow = new System.Windows.Forms.RichTextBox();
            this.button_printBlock = new System.Windows.Forms.Button();
            this.textBox_BlockIndex = new System.Windows.Forms.TextBox();
            this.textBox_publicKey = new System.Windows.Forms.TextBox();
            this.textBox_privateKey = new System.Windows.Forms.TextBox();
            this.button_generateWallet = new System.Windows.Forms.Button();
            this.button_validateKeys = new System.Windows.Forms.Button();
            this.label_publicKey = new System.Windows.Forms.Label();
            this.label_privateKey = new System.Windows.Forms.Label();
            this.button_createTransaction = new System.Windows.Forms.Button();
            this.label_amount = new System.Windows.Forms.Label();
            this.label_fee = new System.Windows.Forms.Label();
            this.textBox_amount = new System.Windows.Forms.TextBox();
            this.textBox_fee = new System.Windows.Forms.TextBox();
            this.label_receiverKey = new System.Windows.Forms.Label();
            this.textBox_receiverKey = new System.Windows.Forms.TextBox();
            this.button_generateNewBlock = new System.Windows.Forms.Button();
            this.button_ReadAll = new System.Windows.Forms.Button();
            this.button_ReadPendingTransactions = new System.Windows.Forms.Button();
            this.button_BlockchainValidation = new System.Windows.Forms.Button();
            this.button_CheckBalance = new System.Windows.Forms.Button();
            this.button_badBlock = new System.Windows.Forms.Button();
            this.button_GenerateRandomData = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.greedy_radioButton = new System.Windows.Forms.RadioButton();
            this.altruistic_radioButton = new System.Windows.Forms.RadioButton();
            this.random_radioButton = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.addressspecific_button = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // mainBlackWindow
            // 
            this.mainBlackWindow.BackColor = System.Drawing.SystemColors.InfoText;
            this.mainBlackWindow.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mainBlackWindow.Location = new System.Drawing.Point(12, 12);
            this.mainBlackWindow.Name = "mainBlackWindow";
            this.mainBlackWindow.Size = new System.Drawing.Size(657, 314);
            this.mainBlackWindow.TabIndex = 0;
            this.mainBlackWindow.Text = "";
            // 
            // button_printBlock
            // 
            this.button_printBlock.Font = new System.Drawing.Font("Segoe UI", 7.5F);
            this.button_printBlock.Location = new System.Drawing.Point(11, 344);
            this.button_printBlock.Margin = new System.Windows.Forms.Padding(2);
            this.button_printBlock.Name = "button_printBlock";
            this.button_printBlock.Size = new System.Drawing.Size(70, 19);
            this.button_printBlock.TabIndex = 1;
            this.button_printBlock.Text = "Print Block";
            this.button_printBlock.UseVisualStyleBackColor = true;
            this.button_printBlock.Click += new System.EventHandler(this.ButtonPrintBlock_Click);
            // 
            // textBox_BlockIndex
            // 
            this.textBox_BlockIndex.Location = new System.Drawing.Point(86, 344);
            this.textBox_BlockIndex.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_BlockIndex.Name = "textBox_BlockIndex";
            this.textBox_BlockIndex.Size = new System.Drawing.Size(36, 20);
            this.textBox_BlockIndex.TabIndex = 2;
            this.textBox_BlockIndex.TextChanged += new System.EventHandler(this.TextBox1_TextChanged);
            // 
            // textBox_publicKey
            // 
            this.textBox_publicKey.Location = new System.Drawing.Point(262, 345);
            this.textBox_publicKey.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_publicKey.Name = "textBox_publicKey";
            this.textBox_publicKey.Size = new System.Drawing.Size(287, 20);
            this.textBox_publicKey.TabIndex = 3;
            this.textBox_publicKey.TextChanged += new System.EventHandler(this.textBox_publicKey_TextChanged);
            // 
            // textBox_privateKey
            // 
            this.textBox_privateKey.Location = new System.Drawing.Point(262, 367);
            this.textBox_privateKey.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_privateKey.Name = "textBox_privateKey";
            this.textBox_privateKey.Size = new System.Drawing.Size(287, 20);
            this.textBox_privateKey.TabIndex = 4;
            this.textBox_privateKey.TextChanged += new System.EventHandler(this.textBox_privateKey_TextChanged);
            // 
            // button_generateWallet
            // 
            this.button_generateWallet.Font = new System.Drawing.Font("Segoe UI", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_generateWallet.Location = new System.Drawing.Point(552, 345);
            this.button_generateWallet.Margin = new System.Windows.Forms.Padding(2);
            this.button_generateWallet.Name = "button_generateWallet";
            this.button_generateWallet.Size = new System.Drawing.Size(70, 41);
            this.button_generateWallet.TabIndex = 5;
            this.button_generateWallet.Text = "Generate Wallet";
            this.button_generateWallet.UseVisualStyleBackColor = true;
            this.button_generateWallet.Click += new System.EventHandler(this.button_generateWallet_Click);
            // 
            // button_validateKeys
            // 
            this.button_validateKeys.Font = new System.Drawing.Font("Segoe UI", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_validateKeys.Location = new System.Drawing.Point(526, 414);
            this.button_validateKeys.Margin = new System.Windows.Forms.Padding(2);
            this.button_validateKeys.Name = "button_validateKeys";
            this.button_validateKeys.Size = new System.Drawing.Size(96, 21);
            this.button_validateKeys.TabIndex = 6;
            this.button_validateKeys.Text = "Validate Keys";
            this.button_validateKeys.UseVisualStyleBackColor = true;
            this.button_validateKeys.Click += new System.EventHandler(this.button_ValidatePrivateKeys_Click);
            // 
            // label_publicKey
            // 
            this.label_publicKey.AutoSize = true;
            this.label_publicKey.Font = new System.Drawing.Font("Segoe UI", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_publicKey.Location = new System.Drawing.Point(209, 346);
            this.label_publicKey.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_publicKey.Name = "label_publicKey";
            this.label_publicKey.Size = new System.Drawing.Size(53, 14);
            this.label_publicKey.TabIndex = 7;
            this.label_publicKey.Text = "Public Key";
            this.label_publicKey.Click += new System.EventHandler(this.Label1_Click);
            // 
            // label_privateKey
            // 
            this.label_privateKey.AutoSize = true;
            this.label_privateKey.Font = new System.Drawing.Font("Segoe UI", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_privateKey.Location = new System.Drawing.Point(207, 370);
            this.label_privateKey.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_privateKey.Name = "label_privateKey";
            this.label_privateKey.Size = new System.Drawing.Size(55, 14);
            this.label_privateKey.TabIndex = 8;
            this.label_privateKey.Text = "Private Key";
            this.label_privateKey.Click += new System.EventHandler(this.label2_Click);
            // 
            // button_createTransaction
            // 
            this.button_createTransaction.Font = new System.Drawing.Font("Segoe UI", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_createTransaction.Location = new System.Drawing.Point(304, 415);
            this.button_createTransaction.Margin = new System.Windows.Forms.Padding(2);
            this.button_createTransaction.Name = "button_createTransaction";
            this.button_createTransaction.Size = new System.Drawing.Size(78, 45);
            this.button_createTransaction.TabIndex = 9;
            this.button_createTransaction.Text = "Create Transaction";
            this.button_createTransaction.UseVisualStyleBackColor = true;
            this.button_createTransaction.Click += new System.EventHandler(this.button_createTransaction_Click);
            // 
            // label_amount
            // 
            this.label_amount.AutoSize = true;
            this.label_amount.Font = new System.Drawing.Font("Segoe UI", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_amount.Location = new System.Drawing.Point(220, 419);
            this.label_amount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_amount.Name = "label_amount";
            this.label_amount.Size = new System.Drawing.Size(43, 14);
            this.label_amount.TabIndex = 10;
            this.label_amount.Text = "Amount";
            this.label_amount.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // label_fee
            // 
            this.label_fee.AutoSize = true;
            this.label_fee.Font = new System.Drawing.Font("Segoe UI", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_fee.Location = new System.Drawing.Point(238, 444);
            this.label_fee.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_fee.Name = "label_fee";
            this.label_fee.Size = new System.Drawing.Size(22, 14);
            this.label_fee.TabIndex = 11;
            this.label_fee.Text = "Fee";
            // 
            // textBox_amount
            // 
            this.textBox_amount.Location = new System.Drawing.Point(262, 416);
            this.textBox_amount.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_amount.Name = "textBox_amount";
            this.textBox_amount.Size = new System.Drawing.Size(38, 20);
            this.textBox_amount.TabIndex = 12;
            this.textBox_amount.TextChanged += new System.EventHandler(this.textBox_amount_TextChanged);
            // 
            // textBox_fee
            // 
            this.textBox_fee.Location = new System.Drawing.Point(262, 442);
            this.textBox_fee.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_fee.Name = "textBox_fee";
            this.textBox_fee.Size = new System.Drawing.Size(38, 20);
            this.textBox_fee.TabIndex = 13;
            this.textBox_fee.TextChanged += new System.EventHandler(this.textBox_fee_TextChanged);
            // 
            // label_receiverKey
            // 
            this.label_receiverKey.AutoSize = true;
            this.label_receiverKey.Font = new System.Drawing.Font("Segoe UI", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_receiverKey.Location = new System.Drawing.Point(199, 394);
            this.label_receiverKey.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_receiverKey.Name = "label_receiverKey";
            this.label_receiverKey.Size = new System.Drawing.Size(62, 14);
            this.label_receiverKey.TabIndex = 14;
            this.label_receiverKey.Text = "Receiver Key";
            this.label_receiverKey.UseWaitCursor = true;
            this.label_receiverKey.Click += new System.EventHandler(this.label_receiverKey_Click);
            // 
            // textBox_receiverKey
            // 
            this.textBox_receiverKey.Location = new System.Drawing.Point(262, 392);
            this.textBox_receiverKey.Margin = new System.Windows.Forms.Padding(2);
            this.textBox_receiverKey.Name = "textBox_receiverKey";
            this.textBox_receiverKey.Size = new System.Drawing.Size(362, 20);
            this.textBox_receiverKey.TabIndex = 15;
            this.textBox_receiverKey.TextChanged += new System.EventHandler(this.textBox_receiverKey_TextChanged);
            // 
            // button_generateNewBlock
            // 
            this.button_generateNewBlock.Font = new System.Drawing.Font("Segoe UI", 7.5F);
            this.button_generateNewBlock.Location = new System.Drawing.Point(12, 367);
            this.button_generateNewBlock.Margin = new System.Windows.Forms.Padding(2);
            this.button_generateNewBlock.Name = "button_generateNewBlock";
            this.button_generateNewBlock.Size = new System.Drawing.Size(70, 37);
            this.button_generateNewBlock.TabIndex = 16;
            this.button_generateNewBlock.Text = "Generate New Block";
            this.button_generateNewBlock.UseVisualStyleBackColor = true;
            this.button_generateNewBlock.Click += new System.EventHandler(this.button_generateNewBlock_Click);
            // 
            // button_ReadAll
            // 
            this.button_ReadAll.Font = new System.Drawing.Font("Segoe UI", 7.5F);
            this.button_ReadAll.Location = new System.Drawing.Point(125, 344);
            this.button_ReadAll.Margin = new System.Windows.Forms.Padding(2);
            this.button_ReadAll.Name = "button_ReadAll";
            this.button_ReadAll.Size = new System.Drawing.Size(70, 19);
            this.button_ReadAll.TabIndex = 17;
            this.button_ReadAll.Text = "Read All";
            this.button_ReadAll.UseVisualStyleBackColor = true;
            this.button_ReadAll.Click += new System.EventHandler(this.button_ReadAll_Click);
            // 
            // button_ReadPendingTransactions
            // 
            this.button_ReadPendingTransactions.Font = new System.Drawing.Font("Segoe UI", 7.5F);
            this.button_ReadPendingTransactions.Location = new System.Drawing.Point(86, 367);
            this.button_ReadPendingTransactions.Margin = new System.Windows.Forms.Padding(2);
            this.button_ReadPendingTransactions.Name = "button_ReadPendingTransactions";
            this.button_ReadPendingTransactions.Size = new System.Drawing.Size(110, 37);
            this.button_ReadPendingTransactions.TabIndex = 18;
            this.button_ReadPendingTransactions.Text = "Read Pending Transactions";
            this.button_ReadPendingTransactions.UseVisualStyleBackColor = true;
            this.button_ReadPendingTransactions.Click += new System.EventHandler(this.button_ReadPendingTransactions_Click);
            // 
            // button_BlockchainValidation
            // 
            this.button_BlockchainValidation.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_BlockchainValidation.Location = new System.Drawing.Point(426, 439);
            this.button_BlockchainValidation.Margin = new System.Windows.Forms.Padding(2);
            this.button_BlockchainValidation.Name = "button_BlockchainValidation";
            this.button_BlockchainValidation.Size = new System.Drawing.Size(196, 37);
            this.button_BlockchainValidation.TabIndex = 19;
            this.button_BlockchainValidation.Text = "Full Blockchain Validation";
            this.button_BlockchainValidation.UseVisualStyleBackColor = true;
            this.button_BlockchainValidation.Click += new System.EventHandler(this.button_BlockchainValidation_Click);
            // 
            // button_CheckBalance
            // 
            this.button_CheckBalance.Font = new System.Drawing.Font("Segoe UI", 7.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_CheckBalance.Location = new System.Drawing.Point(426, 414);
            this.button_CheckBalance.Margin = new System.Windows.Forms.Padding(2);
            this.button_CheckBalance.Name = "button_CheckBalance";
            this.button_CheckBalance.Size = new System.Drawing.Size(96, 21);
            this.button_CheckBalance.TabIndex = 20;
            this.button_CheckBalance.Text = "Check Balance";
            this.button_CheckBalance.UseVisualStyleBackColor = true;
            this.button_CheckBalance.Click += new System.EventHandler(this.button_CheckBalance_Click);
            // 
            // button_badBlock
            // 
            this.button_badBlock.Font = new System.Drawing.Font("Segoe UI", 7.5F);
            this.button_badBlock.Location = new System.Drawing.Point(12, 409);
            this.button_badBlock.Margin = new System.Windows.Forms.Padding(2);
            this.button_badBlock.Name = "button_badBlock";
            this.button_badBlock.Size = new System.Drawing.Size(70, 63);
            this.button_badBlock.TabIndex = 21;
            this.button_badBlock.Text = "Generate New Invalid Block";
            this.button_badBlock.UseVisualStyleBackColor = true;
            this.button_badBlock.Click += new System.EventHandler(this.button_badBlock_Click);
            // 
            // button_GenerateRandomData
            // 
            this.button_GenerateRandomData.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.button_GenerateRandomData.Font = new System.Drawing.Font("Segoe UI", 7.5F);
            this.button_GenerateRandomData.Location = new System.Drawing.Point(86, 410);
            this.button_GenerateRandomData.Margin = new System.Windows.Forms.Padding(2);
            this.button_GenerateRandomData.Name = "button_GenerateRandomData";
            this.button_GenerateRandomData.Size = new System.Drawing.Size(70, 63);
            this.button_GenerateRandomData.TabIndex = 22;
            this.button_GenerateRandomData.Text = "Generate Random Blockchain Data";
            this.button_GenerateRandomData.UseVisualStyleBackColor = false;
            this.button_GenerateRandomData.Click += new System.EventHandler(this.button_GenerateRandomData_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Segoe UI", 7.5F);
            this.button1.Location = new System.Drawing.Point(674, 12);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 60);
            this.button1.TabIndex = 23;
            this.button1.Text = "(Threaded) Generate 5 New Blocks";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Segoe UI", 7.5F);
            this.button2.Location = new System.Drawing.Point(674, 77);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(104, 67);
            this.button2.TabIndex = 24;
            this.button2.Text = "Create Block  Non-Threaded vs. Threaded";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // greedy_radioButton
            // 
            this.greedy_radioButton.AutoSize = true;
            this.greedy_radioButton.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.greedy_radioButton.Location = new System.Drawing.Point(639, 392);
            this.greedy_radioButton.Margin = new System.Windows.Forms.Padding(2);
            this.greedy_radioButton.Name = "greedy_radioButton";
            this.greedy_radioButton.Size = new System.Drawing.Size(86, 23);
            this.greedy_radioButton.TabIndex = 25;
            this.greedy_radioButton.Text = "2. Greedy";
            this.greedy_radioButton.UseVisualStyleBackColor = true;
            this.greedy_radioButton.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // altruistic_radioButton
            // 
            this.altruistic_radioButton.AutoSize = true;
            this.altruistic_radioButton.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.altruistic_radioButton.Location = new System.Drawing.Point(639, 416);
            this.altruistic_radioButton.Margin = new System.Windows.Forms.Padding(2);
            this.altruistic_radioButton.Name = "altruistic_radioButton";
            this.altruistic_radioButton.Size = new System.Drawing.Size(95, 23);
            this.altruistic_radioButton.TabIndex = 26;
            this.altruistic_radioButton.Text = "3. Altruistic";
            this.altruistic_radioButton.UseVisualStyleBackColor = true;
            this.altruistic_radioButton.CheckedChanged += new System.EventHandler(this.altruistic_radioButton_CheckedChanged);
            // 
            // random_radioButton
            // 
            this.random_radioButton.AutoSize = true;
            this.random_radioButton.Checked = true;
            this.random_radioButton.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.random_radioButton.Location = new System.Drawing.Point(639, 365);
            this.random_radioButton.Margin = new System.Windows.Forms.Padding(2);
            this.random_radioButton.Name = "random_radioButton";
            this.random_radioButton.Size = new System.Drawing.Size(93, 23);
            this.random_radioButton.TabIndex = 27;
            this.random_radioButton.TabStop = true;
            this.random_radioButton.Text = "1. Random";
            this.random_radioButton.UseVisualStyleBackColor = true;
            this.random_radioButton.CheckedChanged += new System.EventHandler(this.random_radioButton_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(635, 340);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 20);
            this.label1.TabIndex = 28;
            this.label1.Text = "Mining Setting";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click_2);
            // 
            // addressspecific_button
            // 
            this.addressspecific_button.AutoSize = true;
            this.addressspecific_button.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.addressspecific_button.Location = new System.Drawing.Point(639, 442);
            this.addressspecific_button.Margin = new System.Windows.Forms.Padding(2);
            this.addressspecific_button.Name = "addressspecific_button";
            this.addressspecific_button.Size = new System.Drawing.Size(139, 23);
            this.addressspecific_button.TabIndex = 29;
            this.addressspecific_button.Text = "4. Address Specific";
            this.addressspecific_button.UseVisualStyleBackColor = true;
            this.addressspecific_button.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged_2);
            // 
            // BlockchainApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(789, 481);
            this.Controls.Add(this.addressspecific_button);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.random_radioButton);
            this.Controls.Add(this.altruistic_radioButton);
            this.Controls.Add(this.greedy_radioButton);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button_GenerateRandomData);
            this.Controls.Add(this.button_badBlock);
            this.Controls.Add(this.button_CheckBalance);
            this.Controls.Add(this.button_BlockchainValidation);
            this.Controls.Add(this.button_ReadPendingTransactions);
            this.Controls.Add(this.button_ReadAll);
            this.Controls.Add(this.button_generateNewBlock);
            this.Controls.Add(this.textBox_receiverKey);
            this.Controls.Add(this.label_receiverKey);
            this.Controls.Add(this.textBox_fee);
            this.Controls.Add(this.textBox_amount);
            this.Controls.Add(this.label_fee);
            this.Controls.Add(this.label_amount);
            this.Controls.Add(this.button_createTransaction);
            this.Controls.Add(this.label_privateKey);
            this.Controls.Add(this.label_publicKey);
            this.Controls.Add(this.button_validateKeys);
            this.Controls.Add(this.button_generateWallet);
            this.Controls.Add(this.textBox_privateKey);
            this.Controls.Add(this.textBox_publicKey);
            this.Controls.Add(this.textBox_BlockIndex);
            this.Controls.Add(this.button_printBlock);
            this.Controls.Add(this.mainBlackWindow);
            this.ForeColor = System.Drawing.Color.Black;
            this.Name = "BlockchainApp";
            this.Text = "27007990 - Blockchain App";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox mainBlackWindow;
        private System.Windows.Forms.Button button_printBlock;
        private System.Windows.Forms.TextBox textBox_BlockIndex;
        private System.Windows.Forms.TextBox textBox_publicKey;
        private System.Windows.Forms.TextBox textBox_privateKey;
        private System.Windows.Forms.Button button_generateWallet;
        private System.Windows.Forms.Button button_validateKeys;
        private System.Windows.Forms.Label label_publicKey;
        private System.Windows.Forms.Label label_privateKey;
        private System.Windows.Forms.Button button_createTransaction;
        private System.Windows.Forms.Label label_amount;
        private System.Windows.Forms.Label label_fee;
        private System.Windows.Forms.TextBox textBox_amount;
        private System.Windows.Forms.TextBox textBox_fee;
        private System.Windows.Forms.Label label_receiverKey;
        private System.Windows.Forms.TextBox textBox_receiverKey;
        private System.Windows.Forms.Button button_generateNewBlock;
        private System.Windows.Forms.Button button_ReadAll;
        private System.Windows.Forms.Button button_ReadPendingTransactions;
        private System.Windows.Forms.Button button_BlockchainValidation;
        private System.Windows.Forms.Button button_CheckBalance;
        private System.Windows.Forms.Button button_badBlock;
        private System.Windows.Forms.Button button_GenerateRandomData;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.RadioButton greedy_radioButton;
        private System.Windows.Forms.RadioButton altruistic_radioButton;
        private System.Windows.Forms.RadioButton random_radioButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton addressspecific_button;
    }
}

