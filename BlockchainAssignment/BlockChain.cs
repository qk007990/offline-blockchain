﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlockchainAssignment
{
    class BlockChain
    {
        public List<Block> Blocks = new List<Block>();
        public List<Transaction> TransactionPool = new List<Transaction>();
        public List<Wallet.Wallet> allWallets = new List<Wallet.Wallet>();
        public BlockChain()
        {
            Console.WriteLine("Blockchain created : Genesis Block");
            Blocks.Add(new Block());
        }
        
        public String getBlockInformation(int blockIndex)
        {
            for(int i = 0; i < Blocks.Count; i++)
            {
                if(i == blockIndex)
                {
                    return Blocks[i].Read();
                }
            }
            return "No Block at index " + blockIndex + " was found";
        }

        public String validateBlockchain(Boolean inDepth)
        {
            String validationInfo = "";
            String temp = "";

            for (int i = 1; i < Blocks.Count; i++)
            {
                validationInfo += $"Block {i}\n";
                Block currentBlock = Blocks[i];
                currentBlock.timestamp = Blocks[i].timestamp;
                Block prevBlock = Blocks[i - 1];
                validationInfo += "\tPrevHash valid: ";
                // Check that the current block hash matches previous block hash.
                if (currentBlock.prevHash != prevBlock.hash)
                {
                    //validationInfo += $"Hashes invalid\nCurrent Block:\n{currentBlock.Read()}\nPrevious Block\n{prevBlock.Read()}";
                    validationInfo += "\t\t✗\n";
                } 
                else
                {
                    validationInfo += "\t\t✓\n";
                }

                Block clone = new Block(currentBlock);
                validationInfo += "\tCurrent hash valid: ";
                // Checks if the current block's hash is the same as the newly generated hash with all the blocks current information.
                if (clone.hash != currentBlock.hash)
                {
                    validationInfo += "\t\t✗\n";
                    //return $"Incorrect\n{currentBlock.Read()}\nCorrect\n{Blocks[i].Read()}";
                }
                else 
                {
                    validationInfo += "\t\t✓\n";
                }

                validationInfo += "\tMerkle Root: ";
                // Checks if the merkle root of the current block is valid via recalculation
                if (Block.calculateMerkleRoot(currentBlock.transactionList) != currentBlock.merkleRoot)
                {
                    validationInfo += "\t\t✗\n";
                    // return $"Invalid merkleroot." +
                    //    $"{currentBlock.transactionList.Count}" + 
                    //    $"\nWhat it is: {currentBlock.merkleRoot}\nWhat it should be: {Block.calculateMerkleRoot(currentBlock.transactionList)}";
                }
                else
                {
                    validationInfo += "\t\t✓\n";
                }

                // Checks if Signatures for all transactions in the current block are valid
                foreach (Transaction t in currentBlock.transactionList)
                {
                    if(Wallet.Wallet.ValidateSignature(t.RecipientAddress, t.Hash, t.Signature))
                    {
                        validationInfo += $"\tTransaction {currentBlock.transactionList.IndexOf(t)} Signature: \t✗\n";
                        //return $"Invalid Transaction Signature at Block {i}, Transaction {currentBlock.transactionList.IndexOf(t)}";
                    }
                    else
                    {
                        validationInfo += $"\tTransaction {currentBlock.transactionList.IndexOf(t)} Signature: \t✓\n";
                    }
                   
                }
                if (!inDepth)
                {
                    // If there's nothing wrong with this block, reset string
                    if(validationInfo.Contains("✗") == false)
                    {
                        validationInfo = "";
                    }
                    else
                    {
                        // Store incorrect info to temp
                        temp += validationInfo;
                        validationInfo = "";
                    }
                    // If for loop is on last iteration
                    if(i == Blocks.Count-1)
                    {
                        validationInfo = temp;
                    }
                    Console.WriteLine(validationInfo);
                }
            }
            if(!inDepth)
            {
                if (validationInfo.Contains("✗"))
                {
                    return $"{validationInfo}\nInvalid Blockchain";
                } else
                {
                    return "Valid Blockchain";
                }
            }
            return validationInfo;
        }

        public Block GetLastBlock()
        {
            return Blocks[Blocks.Count - 1];
        }

        public String getAllBlocksInformation()
        {
            String allBlockInformation = "";
            for(int i = 0; i < Blocks.Count; i++)
            {
                allBlockInformation += getBlockInformation(i) + "\n\n";
            }
            return allBlockInformation;
        }
    }
}
