﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BlockchainAssignment
{
    class Block
    {
        public DateTime timestamp;
        public int index;
        public int prevIndex;
        public String hash;
        public String prevHash;
        public String minerAddress;
        public String merkleRoot;
        public List<Transaction> transactionList = new List<Transaction>();
        public int nonce = 0;
        public static float difficulty = 5;
        public float reward;
        public float fee;
        public Boolean hashFound = false;
        
        // Genesis Block (First block of blockchain)
        public Block()
        {
            prevHash = "";
            prevIndex = 0;
            index = 0;
            //Mine();
            timestamp = DateTime.Now;
            ThreadMine();
        }
        
        public Block(String minerAddress, List<Transaction> transactions, Block lastBlock)
        {
            // Initial reward value.
            this.reward = 100;
            // Add transactions from transaction pool to newly generated blocks. (before hash)
            transactionList = transactions;
            this.minerAddress = minerAddress;
            // Adds a reward transaction to transactionList
            transactionList.Add(calculateReward());
            timestamp = DateTime.Now;
            //Mine();
            this.merkleRoot = calculateMerkleRoot(transactionList);
            index = lastBlock.index + 1;
            // Hash of prior block
            prevHash = lastBlock.hash;
            // Index of prior block
            prevIndex = lastBlock.index;
            //ThreadMine();
            Mine();
        }

        // Clone block constructor
        public Block(Block block)
        {
            // Initial reward value.
            this.reward = block.reward;
            // Add transactions from transaction pool to newly generated blocks. (before hash)
            transactionList = block.transactionList;
            this.minerAddress = block.minerAddress;
            // Adds a reward transaction to transactionList
            transactionList.Add(calculateReward());
            timestamp = block.timestamp;
            // Recalculates hash
            Mine();
            // Recalculates merkle root
            this.merkleRoot = calculateMerkleRoot(transactionList);
            index = block.index;
            // Hash of prior block
            prevHash = block.prevHash;
            // Index of prior block
            prevIndex = block.prevIndex;
        }

        private String CreateHash()
        {
            SHA256 hasher = SHA256Managed.Create();
            String input = index.ToString() + timestamp.ToString() + prevHash + nonce + difficulty + reward + merkleRoot; ;

            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes((input)));
            
            String hash = string.Empty;

            foreach (byte x in hashByte)
            {
                hash += String.Format("{0:x2}", x);
            }
            return hash;
        }

        private void Mine()
        {
            Console.WriteLine("Mining: Non-Threaded");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            // Generate string with as many zeroes at the length of the difficulty value
            String hashZeros = "";
            for(int i = 0; i < difficulty; i++)
            {
                hashZeros += "0";
            }
            String h = "";
            while (!h.StartsWith(hashZeros))
            {
                // Iterate nonce value every loop the hash does not begin with the string value stored in hashZeroes
                nonce += 1;
                h = CreateHash();
            }
            //Console.WriteLine(index.ToString() + "\n" + timestamp.ToString() + "\n" + prevHash + "\n" + nonce + "\n" + difficulty + "\n" + reward + "\n" + merkleRoot);
            hash = h;
            sw.Stop();
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = sw.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}.{1:00}", ts.Seconds, ts.Milliseconds/10);
            // Returns hash that fulfils the difficulty rule
            Console.WriteLine("Took " + elapsedTime + "s to generate hash.\nDifficulty: " + difficulty + $"\nNonce {nonce}\nHash: {this.hash}\n");
        }

        /**
         * Reference: https://docs.microsoft.com/en-us/dotnet/api/system.threading.tasks.parallel?view=net-5.0
         * Parallel loop using tasks to get hash - not a great solution as it finds other hashes that satisfy the difficulty
         */
        private void ParallelLoopMine()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int N = 100000000;
            String temp = "";
            String hashZeros = "";
            List<String> tempHashes = new List<string>();
            for(int i = 0; i < difficulty; i++)
            {
                hashZeros += "0";
            }
            Parallel.For(0, N, (i, state) =>
            {
                // Increment nonce value before generating hash.
                if (!temp.StartsWith(hashZeros))
                {
                    nonce += 1;
                    if (nonce != 0 && nonce % 1000000 == 0)
                    {
                        Console.WriteLine("PMine Nonce: " + nonce + "\t" + i);
                    }
                    temp = !temp.StartsWith(hashZeros) ? CreateHash() : temp;
                } else if (temp.StartsWith(hashZeros) || !this.hashFound) 
                {
                    // hash has been found
                    if(tempHashes.Count < 1)
                    {
                        tempHashes.Add(temp);
                        Console.WriteLine("Found: " + temp);
                        this.hashFound = true;
                        Console.WriteLine("Found2: " + temp);
                    }
                    // Break out of this threaded for loop
                    state.Break();
                    sw.Stop();
                } 
            });
            Console.WriteLine("No Of Hashes: " + tempHashes.Count);
            Console.WriteLine(index.ToString() + "\n" + timestamp.ToString() + "\n" + prevHash + "\n" + nonce + "\n" + difficulty + "\n" + reward + "\n" + merkleRoot);
            this.hash = tempHashes[0];
            Console.WriteLine("Hash: " + this.hash + "\ntempHashes[0]" + tempHashes[0]);
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = sw.Elapsed;
            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}.{1:00}", ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("Took " + elapsedTime + "s to generate hash.\nDifficulty: " + difficulty + $"\nNonce {nonce}\nHash: {this.hash}");
        }

        public delegate void ParamaterizedThreadStart(object obj);
        private void ThreadMine()
        {
            Console.WriteLine("Mining: Threaded");
            //Console.WriteLine($"\nThreading starting\nBlock:\n{this.Read()}");
            String hashZeros = "";
            for (int i = 0; i < difficulty; i++)
            {
                hashZeros += "0";
            }
            hashFound = false;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int tempNonce = 0;
            Thread[] threads = new Thread[Environment.ProcessorCount];
            // For every thread (In my case, 4)
            for (int i = 0; i < threads.Length; i++)
            {
                // Assign each thread this function
                threads[i] = new Thread(() =>
                {
                    String tempHash = "";
                    while (!tempHash.StartsWith(hashZeros))
                    {
                        if(!hashFound)
                        {
                            nonce += hashFound ? 0 : 1;
                            tempHash = CreateHash();
                            tempNonce = tempHash.StartsWith(hashZeros) ? nonce : 0;
                        }
                        else if(tempHash.StartsWith(hashZeros))
                        {
                            //tempNonce = nonce;
                            break;
                        }
                        else
                        {
                            return;
                        }
                    }
                    
                    if (tempHash.StartsWith(hashZeros) && !hashFound)
                    {
                        hash = tempHash;
                        nonce = tempNonce;
                        // Console.WriteLine($"Found hash: {hash}");
                        this.hashFound = true;
                        return;
                    }
                    
                });
                //threads[i].Name = "Thread " + i;
            };
            foreach(Thread t in threads)
            {
                t.Start();
            }
            foreach (Thread t in threads)
            {
                t.Join();
            }
            //Console.WriteLine($"Beginning Nonce: {nonce}\t Hash: {this.hash}");
            sw.Stop();
            // Get the elapsed time as a TimeSpan value.
            TimeSpan ts = sw.Elapsed;
            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}.{1:00}", ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine("Took " + elapsedTime + "s to generate hash.\nDifficulty: " + difficulty + $"\nNonce {nonce}\nHash: {this.hash}\n");
        }
        /**
         * Generates hashes using the same information but one method uses threading, the other doesn't.
         * Mine() : Non-Threaded
         * ParallelLoopMine() : Threaded
         */
        public void testMiningFunctions()
        {
            this.hash = "";
            nonce = 0;
            Console.WriteLine("Non-Threaded vs. Threaded");
            Mine();
            this.hash = "";
            nonce = 0;
            ParallelLoopMine();
            Console.WriteLine("Testing done");
        }
        
        public void testThreadMining()
        {
            // Prints this blocks information to console
            Console.WriteLine(this.Read());
            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            // Reset nonce value to 0
            this.nonce = 0;
            // Reset hash to nothing
            this.hash = "";
            Console.WriteLine("%%%%%% Thread %%%%%%%");
            // Generate hash via ThreadMine function
            ThreadMine();
            // Read block's new info (Should match the previously read block info every time since the block info is the same)
            Console.WriteLine($"{this.Read()}");
            Console.WriteLine("%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            //Console.WriteLine($"Hash found: {this.hash}\nNonce: {nonce}\nThreaded mining finished");
        }
        
        public void rehash()
        {
            String oldHash = this.hash;
            Mine();
            Console.WriteLine($"Old hash: {oldHash}\nNew Hash: {this.hash}");
        }

        private Transaction calculateReward()
        {
            // For every transaction's amount and fee, add to rewardAndFees
            if (transactionList.Count > 0)
            {
                foreach (Transaction t in transactionList)
                {
                    // Append this transactions fee value to reward
                    this.reward += t.Fee;
                    // This blocks transaction fees
                    this.fee += t.Fee;
                }
            }
            // Add collective amounts to transactionList
            return new Transaction("Mine Rewards", minerAddress, this.reward, 0, "");
        }

        public static String calculateMerkleRoot(List<Transaction> transactionList)
        {
            // Initialise a list of all hashes from this blocks transactions
            List<String> hashes = new List<String>();
            foreach(Transaction t in transactionList)
            {
                hashes.Add(t.Hash);
            }
            // To temporarily store newly generated hashes
            List<String> tempHashes = new List<String>();
            String tempHash = "";

            // If only one hash, return hash result from combineHash function
            if (hashes.Count == 1) 
                return HashCode.HashTools.combineHash(hashes[0], hashes[0]);

            // Continue inner code until hashes list only has 1 element (Merkle Root)
            while(hashes.Count != 1)
            {
                for(int i = 0; i < hashes.Count; i+=2)
                {
                    if(i == (hashes.Count -1))
                    {
                        tempHash = HashCode.HashTools.combineHash(hashes[i], hashes[i]);
                    }
                    else
                    {
                        tempHash = HashCode.HashTools.combineHash(hashes[i], hashes[i + 1]);
                    }
                    // Add new temphash to temphashes array
                    tempHashes.Add(tempHash);
                }
                // Hashes becomes tempHashes (Contains the newly generated hashes)
                hashes = tempHashes;
            }
            // Will return merkle root once there's only one element left in the hashes array
            return hashes[0];
        }

        public String Read()
        {
            String output = "";
            output += "Block Index: " + this.index +
                        "\tTimeStamp: " + this.timestamp +
                        "\nHash:\t\t" + this.hash +
                        "\nMerkle Root:\t" + this.merkleRoot +
                        "\nPrevious Hash:\t" + this.prevHash +
                        "\nNonce:\t" + this.nonce +
                        "\nDifficulty:\t" + difficulty +
                        "\nReward:\t" + this.reward + "\tFees:\t" + this.fee +
                        "\nTotal Reward:\t" + (this.reward + this.fee) +
                        "\nTransactions:\t" + this.transactionList.Count + "\n";


            //foreach (Transaction t in transactionList)
            //{
            //    output += "\n" + t.ReadTransaction();
            //}
            return output;
                   
        }
    }
}
