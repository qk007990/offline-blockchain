# Offline Blockchain
This is my Offline Blockchain Project. Apologies for not having uploaded it to GitLab sooner. I've developed this project via Parallels VM software on Mac - It should theoretically work with no issues.

Found the project very interesting and fun - if I wasn't so bombarded with assignment work, I would definitely have pursued more conventional methods at tackling the tasks and would have attempted to get a automated/simulated blockchain environment going.

Well structured coursework. Cheers!
